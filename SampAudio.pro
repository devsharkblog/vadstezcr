#-------------------------------------------------
#
# Project created by QtCreator 2015-06-22T13:09:56
#
#-------------------------------------------------

QT       += core gui multimedia printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SampAudio
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    qtvad.cpp \
    libs/QCustomPlot/qcustomplot.cpp \
    libs/QCustomPlot/qcustomplotwrap.cpp

HEADERS  += mainwindow.h \
    qtvad.h \
    libs/QCustomPlot/qcustomplot.h \
    libs/QCustomPlot/qcustomplotwrap.h

FORMS    += mainwindow.ui

DISTFILES += \
    Readme.md
