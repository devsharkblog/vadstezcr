#include "qcustomplotwrap.h"

QCustomPlotWrap::QCustomPlotWrap()
{

}

void QCustomPlotWrap::plotUpdate(QCustomPlot *plot,
                                 QVector<QCustomPlotWrap_graphValues> &graphValues,
                                 double x_min,
                                 double x_max,
                                 double y_min,
                                 double y_max)
{
    //  create colors  //
    QStringList colors;
    colors.append("#607234");
    colors.append("#460846");
    colors.append("#452398");
    colors.append("#435345");
    colors.append("#678678");
    colors.append("#286736");
    colors.append("#876873");
    colors.append("#564787");
    colors.append("#123312");
    colors.append("#455400");

    //  clear old graphs  //
    plot->clearGraphs();

    //  set axis labels  //
    plot->xAxis->setLabel("Keys (s)");
    plot->yAxis->setLabel("Values (m)");

    //  change range of axis 2, when changing axis 1  //
    QObject::connect(plot->xAxis,
                     SIGNAL(rangeChanged(QCPRange)),
                     plot->xAxis2,
                     SLOT(setRange(QCPRange)));

    QObject::connect(plot->yAxis,
                     SIGNAL(rangeChanged(QCPRange)),
                     plot->yAxis2,
                     SLOT(setRange(QCPRange)));

    //  set interaction: drag and zoom  //
    plot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);

    //  set visible axis 2  //
    plot->xAxis2->setVisible(true);
    plot->yAxis2->setVisible(true);

    //  remove tick labels from axes 2  //
    plot->xAxis2->setTickLabels(false);
    plot->yAxis2->setTickLabels(false);

    //  add graphs  //
    for(int i = 0; i < graphValues.count(); i++)
    {
        //  add graph  //
        plot->addGraph();
        plot->graph(i)->setData(graphValues.at(i).keys, graphValues.at(i).values);

        //  set color  //
        QPen pen;
        pen.setColor(QColor(colors.at(i % 10)));
        plot->graph(i)->setPen(pen);
    }

    //  set range for axis (with 10 percent offset)  //
    plot->xAxis->setRange(x_min - 0.1 * (x_max - x_min),
                          x_max + 0.1 * (x_max - x_min));
    plot->yAxis->setRange(y_min - 0.1 * (y_max - y_min),
                          y_max + 0.1 * (y_max - y_min));

    //  repaint  //
    plot->replot();
}

void QCustomPlotWrap::plotUpdate(QCustomPlot *plot,
                                 QVector<QCustomPlotWrap_graphValues> &graphValues)
{
    if(graphValues.isEmpty())
    {
        qDebug() << "Error: No graphs";

        return;
    }

    //  to prevent null pointer above  //
    if(graphValues.at(0).keys.isEmpty() || graphValues.at(0).values.isEmpty())
    {
        qDebug() << "Error: Empty data in graph";

        return;
    }

    //  keys  //

    double keysMin = graphValues.at(0).keys.at(0);
    double keysMax = graphValues.at(0).keys.at(0);

    //  foreach graph  //
    for(int i = 0; i < graphValues.count(); i++)
    {
        //  foreach key  //
        for(int j = 0; j < graphValues.at(i).keys.count(); j++)
        {
            //  catch min  //
            if(graphValues.at(i).keys.at(j) < keysMin)
            {
                keysMin = graphValues.at(i).keys.at(j);
            }

            //  catch max  //
            if(graphValues.at(i).keys.at(j) > keysMax)
            {
                keysMax = graphValues.at(i).keys.at(j);
            }
        }
    }

    //  values  //

    double valuesMin = graphValues.at(0).values.at(0);
    double valuesMax = graphValues.at(0).values.at(0);

    //  foreach graph  //
    for(int i = 0; i < graphValues.count(); i++)
    {
        //  foreach value  //
        for(int j = 0; j < graphValues.at(i).values.count(); j++)
        {
            //  catch min  //
            if(graphValues.at(i).values.at(j) < valuesMin)
            {
                valuesMin = graphValues.at(i).values.at(j);
            }

            //  catch max  //
            if(graphValues.at(i).values.at(j) > valuesMax)
            {
                valuesMax = graphValues.at(i).values.at(j);
            }
        }
    }

    //  update plot  //
    plotUpdate(plot,
               graphValues,
               keysMin,
               keysMax,
               valuesMin,
               valuesMax);
}
