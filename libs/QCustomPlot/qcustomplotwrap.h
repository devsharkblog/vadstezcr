#ifndef QCUSTOMPLOTWRAP_H
#define QCUSTOMPLOTWRAP_H

#include "qcustomplot.h"

struct QCustomPlotWrap_graphValues
{
    QVector<double> keys;
    QVector<double> values;
};

class QCustomPlotWrap
{
public:
    QCustomPlotWrap();

    static void plotUpdate(QCustomPlot *plot,
                    QVector<QCustomPlotWrap_graphValues> &graphValues,
                    double x_min,
                    double x_max,
                    double y_min,
                    double y_max);

    static void plotUpdate(QCustomPlot *plot,
                    QVector<QCustomPlotWrap_graphValues> &graphValues);
};

#endif // QCUSTOMPLOTWRAP_H
