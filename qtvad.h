#ifndef QTVAD_H
#define QTVAD_H

#include <QObject>
#include <QMediaRecorder>
#include <QAudioProbe>
#include <QTimer>
#include <QtMath>
#include <QDateTime>

class QtVAD : public QObject
{
    Q_OBJECT
public:
    explicit QtVAD(QObject *parent = 0);

    bool setSource(QMediaRecorder *source);

    double getSteMax();
    double getZcrMax();

private:
    QAudioProbe *audioProbe;

    double steMax;
    double zcrMax;

    QVector <double> steValues;
    QVector <double> zcrValues;

    double firstNoiseDurationMS;
    QTimer *firstNoiseTimestampTimer;

    QVector <qint32> voiceData;

private:
    long long calc_ste(const qint16 *data, int count);
    long long calc_zcr(const qint16 *data, int count);

signals:
    void voice_started();
    void voice_finished(QVector <qint32> *data);
    void newSection(double ste, double zcr);

private slots:
    void processBuffer(QAudioBuffer audioBuffer);
    void messageEndTimestampTimer_timeout();
};

#endif // QTVAD_H
