#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //  create recorder  //
    audioRecorder = new QAudioRecorder(this);

    //  configure recorder  //
    QAudioEncoderSettings audioSettings;
    audioSettings.setCodec("audio/PCM");
    audioSettings.setQuality(QMultimedia::HighQuality);
    audioSettings.setChannelCount(1);
    audioRecorder->setEncodingSettings(audioSettings);

    //  create VAD  //
    vad = new QtVAD(this);
    connect(vad,
            SIGNAL(voice_started()),
            this,
            SLOT(vad_voice_started()));
    connect(vad,
            SIGNAL(voice_finished(QVector<qint32>*)),
            this,
            SLOT(vad_voice_finished(QVector<qint32>*)));
    connect(vad,
            SIGNAL(newSection(double,double)),
            this,
            SLOT(vad_newSection(double,double)));

    //  set source  //
    if(!vad->setSource(audioRecorder))
    {
        qDebug() << "shit";
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

bool MainWindow::saveWAV(QVector <qint32> *samplesBuffer)
{
    //  configure format  //
    SF_INFO sndFileInfo;
    sndFileInfo.channels = 1;
    sndFileInfo.samplerate = 44100;
    sndFileInfo.format = SF_FORMAT_WAV | SF_FORMAT_PCM_32;

    QString filePath = "/home/mfbmir/customWAV-" + QString::number(QDateTime::currentMSecsSinceEpoch()) + ".wav";

    //  open file  //
    SNDFILE *sndFile = sf_open(filePath.toStdString().c_str(),
                               SFM_WRITE,
                               &sndFileInfo);

    //  check file was opened  //
    if(sndFile != NULL)
    {
        //  write samples  //
        sf_count_t count = sf_write_int(sndFile,
                                        samplesBuffer->data(),
                                        samplesBuffer->count());

        //  debug time  //
        qDebug() << "Written "
                 << count << " items; "
                 << (samplesBuffer->count() / 44100.0) << " seconds";
    }
    else
    {
        //  get error string  //
        QString errStr = sf_strerror(sndFile);

        //  debug error  //
        qDebug() << "Save WAV err: " << errStr;

        return false;
    }

    //  close file  //
    sf_close(sndFile);

    return true;
}

void MainWindow::on_pushButton_start_clicked()
{
    audioRecorder->record();
}

void MainWindow::on_pushButton_stop_clicked()
{
    audioRecorder->stop();
}

void MainWindow::vad_voice_started()
{
    qDebug() << "Speech: START";
}

void MainWindow::vad_voice_finished(QVector<qint32> *data)
{
    qDebug() << "Speech: END";

    saveWAV(data);
}

void MainWindow::vad_newSection(double ste, double zcr)
{
    //  save //
    steArray.append(ste);

    //  save  //
    zcrArray.append(zcr);

    //
    //  update plot STE  //
    //

    //  ste values  //

    QCustomPlotWrap_graphValues graphValuesSte;

    for(int i = 0; i < steArray.count(); i++)
    {
        //  set key  //
        graphValuesSte.keys.append(i);

        //  set value  //
        graphValuesSte.values.append(steArray.at(i));
    }

    //  ste limit  //

    QCustomPlotWrap_graphValues graphValuesSteLimit;

    graphValuesSteLimit.keys.append(0);
    graphValuesSteLimit.keys.append(graphValuesSte.keys.count() - 1);

    graphValuesSteLimit.values.append(vad->getSteMax());
    graphValuesSteLimit.values.append(vad->getSteMax());

    //  all  //

    QVector <QCustomPlotWrap_graphValues> graphsSte;

    graphsSte.append(graphValuesSte);
    graphsSte.append(graphValuesSteLimit);

    //  update plot  //
    QCustomPlotWrap::plotUpdate(ui->plot,
                                graphsSte);

    //
    //  update plot ZCR  //
    //

    //  zcr values  //

    QCustomPlotWrap_graphValues graphValuesZcr;

    for(int i = 0; i < zcrArray.count(); i++)
    {
        //  set key  //
        graphValuesZcr.keys.append(i);

        //  set value  //
        graphValuesZcr.values.append(zcrArray.at(i));
    }

    //  zcr limit  //

    QCustomPlotWrap_graphValues graphValuesZcrLimit;

    graphValuesZcrLimit.keys.append(0);
    graphValuesZcrLimit.keys.append(graphValuesZcr.keys.count() - 1);

    graphValuesZcrLimit.values.append(vad->getZcrMax());
    graphValuesZcrLimit.values.append(vad->getZcrMax());

    //  all  //

    QVector <QCustomPlotWrap_graphValues> graphsZcr;

    graphsZcr.append(graphValuesZcr);
    graphsZcr.append(graphValuesZcrLimit);

    //  update plot  //
    QCustomPlotWrap::plotUpdate(ui->plot2,
                                graphsZcr);
}

