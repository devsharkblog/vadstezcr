#include "qtvad.h"

QtVAD::QtVAD(QObject *parent) : QObject(parent)
{
    //  create probe  //
    audioProbe = new QAudioProbe();
    connect(audioProbe,
            SIGNAL(audioBufferProbed(QAudioBuffer)),
            this,
            SLOT(processBuffer(QAudioBuffer)));

    steMax = 0;
    zcrMax = 0;

    firstNoiseDurationMS = 300;

    firstNoiseTimestampTimer = new QTimer(this);
    connect(firstNoiseTimestampTimer,
            SIGNAL(timeout()),
            this,
            SLOT(messageEndTimestampTimer_timeout()));
}

bool QtVAD::setSource(QMediaRecorder *source)
{
    return audioProbe->setSource(source);
}

double QtVAD::getSteMax()
{
    return steMax;
}

double QtVAD::getZcrMax()
{
    return zcrMax;
}

long long QtVAD::calc_zcr(const qint16 *data, int count)
{
    double zcr = 0;

    for(int i = 0; i < count; i++)
    {
        //  calc zero-crossing rate  //
        if(i > 0)
        {
            if(data[i] * data[i - 1] <= 0)
            {
                zcr++;
            }
        }
    }

    return zcr;
}

long long QtVAD::calc_ste(const qint16 *data, int count)
{
    double squaresSum = 0;

    for(int i = 0; i < count; i++)
    {
        //  calc sum of squares  //
        squaresSum += (double) (data[i]) * (double) (data[i]);
    }

    double ste = squaresSum / count;

    return ste;
}

void QtVAD::processBuffer(QAudioBuffer audioBuffer)
{
    //
    //  Get data  //
    //

    //  get 16bit data  //
    const qint16 *data = audioBuffer.constData<qint16>();

    //
    //  Calc STE, ZRC  //
    //

    //  calc Short Term Energy  //
    double ste = calc_ste(data, audioBuffer.sampleCount());

    //  calc Zero-Crossing Rate  //
    double zcr = calc_zcr(data, audioBuffer.sampleCount());

    //  check ste limit process  //
    if(firstNoiseDurationMS > 0)
    {
        //  decrement ste limit duration  //
        firstNoiseDurationMS -= (double) (audioBuffer.duration() / 1000.0);

        //  add values to buffer  //
        steValues.append(ste);
        zcrValues.append(zcr);

        //  catch last value  //
        if(firstNoiseDurationMS < 0)
        {
            //  set start values  //
            steMax = steValues.at(0);
            zcrMax = zcrValues.at(0);

            //  calc max  //
            for(int i = 0; i < steValues.count(); i++)
            {
                //  find max  //

                //  ste  //
                if(steValues.at(i) > steMax)
                {
                    steMax = steValues.at(i);
                }

                //  zcr  //
                if(zcrValues.at(i) > zcrMax)
                {
                    zcrMax = zcrValues.at(i);
                }
            }

            //  set 10 percent  //
            steMax *= 1.1;
            zcrMax *= 1.1;
        }
    }

    emit newSection(ste, zcr);

    //
    //  Check voice overflow  ///
    //

    //  by ste  //
    if( (ste > steMax) || (zcr > zcrMax) )
    {
        //  catch message start  //
        if(!firstNoiseTimestampTimer->isActive())
        {
            emit voice_started();

            voiceData.clear();
        }

        //  start catching end of message  //
        firstNoiseTimestampTimer->start(1000);
    }

    //
    //  Check voice is active  //
    //

    if(firstNoiseTimestampTimer->isActive())
    {
        //  get 32bit data - better quality  //
        const qint32 *data32b = audioBuffer.constData<qint32>();

        //  add data to buffer  //
        for(int i = 0; i < audioBuffer.sampleCount(); i++)
        {
            voiceData.append(data32b[i]);
        }
    }
}

void QtVAD::messageEndTimestampTimer_timeout()
{
    //  stop timer  //
    firstNoiseTimestampTimer->stop();

    emit voice_finished(&voiceData);
}

