#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QAudioRecorder>
#include <QUrl>
#include <QAudioProbe>
#include <QAudioInput>
#include <QDateTime>

#include <sndfile.h>

#include "libs/QCustomPlot/qcustomplotwrap.h"
#include "qtvad.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    QAudioRecorder *audioRecorder;



    QVector <double> steArray;
    QVector <double> zcrArray;

    QtVAD *vad;

private:
    bool saveWAV(QVector<qint32> *samplesBuffer);

private slots:
    void on_pushButton_start_clicked();
    void on_pushButton_stop_clicked();

    void vad_voice_started();
    void vad_voice_finished(QVector <qint32> *data);
    void vad_newSection(double ste, double zcr);
};

#endif // MAINWINDOW_H
